package sample;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.*;


public class Controller {

    ObservableList<String> operator = FXCollections.observableArrayList(//
            "/", "*", "-", "+");

    @FXML
    private TextField firstnum;
    @FXML
    private TextField secondNum;

    @FXML
    private Label result;


    @FXML
    Spinner<String> spinner = new Spinner<>();


    public void initialize() {
        SpinnerValueFactory<String> valueFactory = //
                new SpinnerValueFactory.ListSpinnerValueFactory<>(operator);

        valueFactory.setValue("+");
        spinner.setValueFactory(valueFactory);
    }

    public void calculate() {

        double num1 = 0;
        double num2 = 0;
        try {
            num1 = Double.parseDouble(firstnum.getText());
            num2 = Double.parseDouble(secondNum.getText());
        } catch (Exception e) {
            result.setText("Введите числа");
            return;
        }
        String operator = spinner.getValue();

        double res;
        switch (operator) {
            case "+":
                res = num1 + num2;
                break;
            case "-":
                res = num1 - num2;
                break;
            case "*":
                res = num1 * num2;
                break;
            case "/":
                if (num2 == 0) {
                    result.setText("На ноль не делим");
                    return;
                } else
                    res = num1 / num2;
                break;
            default:
                res = 0;
                break;
        }
        result.setText(String.valueOf(res));
    }

}
